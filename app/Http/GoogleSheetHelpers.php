<?php
/**
 * Created by PhpStorm.
 * User: tariq
 * Date: 8/9/2018
 * Time: 10:34 PM
 */

namespace App\Http;
use Google_Service_Sheets_BatchUpdateSpreadsheetRequest;
use Google_Service_Sheets_Request;
use Google_Service_Sheets_ValueRange;


class GoogleSheetHelpers
{
    public static function fetchSpreadSheetRecords($service, $sheetId, $sheetColumns, $isHeader = false){

        $result = $service->spreadsheets_values->get($sheetId, $sheetColumns)->getValues();

        return $result;

    }
    public static function fetchKeyWithValues($service, $sheetId, $sheetColumns, $isHeader = false){


        $result = $service->spreadsheets_values->get($sheetId, $sheetColumns);

        return $result;

    }

    public static function appendSheetData($service, $sheetId, $sheetColumns, $data){


        $options = array('valueInputOption' => 'RAW');
        $body   = new Google_Service_Sheets_ValueRange(['values' => $data]);

        $result = $service->spreadsheets_values->append($sheetId, $sheetColumns, $body, $options);
        return true;

    }

    public  static  function  changeRowColor($service, $sheetId, $count, $sheetPageId){

//        $sheetPageId = env('GOOGLE_KIT_LIST_SHEET_PAGE_ID');
        $requests = [
            new Google_Service_Sheets_Request([
                'repeatCell' => [
                    "range"=> [
                        "sheetId"=> $sheetPageId,
                        "startRowIndex"=> $count,
                        "endRowIndex"=> $count+1,
                        "startColumnIndex"=> 0,
                        "endColumnIndex"=> 5,
                    ],
                    "cell"=>[

                        "userEnteredFormat"=>[

                            "textFormat"=>[
                                "foregroundColor"=> [
                                    "red" =>1.0,
                                    "green"=> 0.0,
                                    "blue"=> 0.0
                                ],
//                                      "fontSize"=> 12,
//                                      "bold"=> true
                            ]
                        ]
                    ],
                    "fields"=> "userEnteredFormat(textFormat)"
                ]
            ])
        ];

        $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
            'requests' => $requests
        ]);
        try {
            $response = $service->spreadsheets->batchUpdate($sheetId, $batchUpdateRequest);
        }catch (\Exception $e){
            return "debug";
        }
//        dd($batchUpdateRequest);
    }
    public static function updateSheetData($service, $sheetId, $sheetColumns, $data){



        $options = array('valueInputOption' => 'RAW');
        $body   = new Google_Service_Sheets_ValueRange(['values' => $data]);

        $result = $service->spreadsheets_values->update($sheetId, $sheetColumns, $body, $options);
        return true;

    }

}
