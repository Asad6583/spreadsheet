<?php

namespace App\Http\Controllers;


use App\User;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class GoogleSheetController extends Controller
{
    public $gClient;
    public $readSheetId = '1C6_r_vZa-NwkKYMVvZ7mo1yAL2zTpemN7zsrq6fEJE4';
    public function __construct(){

        $google_redirect_url = route('glogin');
        $this->gClient = new Google_Client();
        $this->gClient->setApplicationName("SheetsApps");
        $this->gClient->setClientId('1020580055552-3mt8d0ghvdvreuctrh6m391ff9u019s2.apps.googleusercontent.com');
        $this->gClient->setClientSecret('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');
        $this->gClient->setRedirectUri($google_redirect_url);
        $this->gClient->setDeveloperKey('AIzaSyC0ptVNwK7EFccpIr_IFJcU25CYop2MIc0');
        $this->gClient->setScopes(array(
            'https://www.googleapis.com/auth/drive.file',
            'https://www.googleapis.com/auth/drive'
        ));
        $this->gClient->setAccessType("offline");
        $this->gClient->setApprovalPrompt("force");
    }

    public function addRow(Request $request){
        $validator = Validator::make($request->all(), [
            'input_one'=>'required',
            'input_two'=>'required',
            'files_data' => 'required'
        ]);

        if ($validator->fails())
        {
            $messages = $validator->getMessageBag();
            return response()->json(['data' => ['error' => true, 'message' => $messages->first()]], 401);
        }

        foreach ($request->files_data as $file){
           $filePath = $this->uploadPhoto($file);
           if($filePath!=false){
               $oneDriveLinks[] = $this->uploadFileOnedrive($filePath);
           }
        }

        $oneDriveLinks = implode(',',$oneDriveLinks);

        $service = new Google_Service_Sheets($this->gClient);
        $getrange = 'A:F';

        $values = [
            [$request->input_one, $request->input_two, $oneDriveLinks],
        ];

        self::appendSheetData($service,$this->readSheetId,$getrange,$values);

        return response()->json(['data' => ['success' => true, 'message' => "Data inserted successfully"]], 201);
    }


    public function uploadPhoto($file)
    {
        if(isset($file)){

            $filename = time() .Str::random(30). '.' . $file->getClientOriginalExtension();

            $file->move(public_path('/uploadFiles'), $filename);

            return $filename;

        }else{
            return false;
        }
    }

    public function googleLogin(Request $request)  {

        $google_oauthV2 = new \Google_Service_Oauth2($this->gClient);
        if ($request->get('code')){
            $this->gClient->authenticate($request->get('code'));
            $request->session()->put('token', $this->gClient->getAccessToken());
        }
        if ($request->session()->get('token'))
        {
            $this->gClient->setAccessToken($request->session()->get('token'));
        }
        if ($this->gClient->getAccessToken())
        {
            //For logged in user, get details from google using acces
            $user=User::find(1);
            $user->access_token=json_encode($request->session()->get('token'));
            $user->save();
            dd("Successfully authenticated");
        } else
        {
            //For Guest user, get google login url
            $authUrl = $this->gClient->createAuthUrl();
            return redirect()->to($authUrl);
        }
    }

    public function uploadFileOnedrive($fileName){

        try {
            $service = new Google_Service_Drive($this->gClient);

            $user=User::find(1);

            $this->gClient->setAccessToken(json_decode($user->access_token,true));

            if ($this->gClient->isAccessTokenExpired()) {
                // save refresh token to some variable
                $refreshTokenSaved = $this->gClient->getRefreshToken();
                // update access token
                $this->gClient->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
                // // pass access token to some variable
                $updatedAccessToken = $this->gClient->getAccessToken();
                // // append refresh token
                $updatedAccessToken['refresh_token'] = $refreshTokenSaved;
                //Set the new acces token
                $this->gClient->setAccessToken($updatedAccessToken);
                $user->access_token=json_encode($updatedAccessToken);
                $user->save();
            }

            $fileMetadata = new Google_Service_Drive_DriveFile(array(
                'name' => 'GoogleSheet',
                'mimeType' => 'application/vnd.google-apps.folder'));
            $folder = $service->files->create($fileMetadata, array(
                'fields' => 'id'));

            $file = new Google_Service_Drive_DriveFile(array(
                'name' => $fileName,
                'parents' => array($folder->id)
            ));
            $result = $service->files->create($file, array(
                'data' => file_get_contents(public_path('uploadFiles/'.$fileName)),
                'mimeType' => 'application/octet-stream',
                'uploadType' => 'media'
            ));
            // get url of uploaded file
            $url='https://drive.google.com/open?id='.$result->id;
            return $url;
        }catch (\Exception $exception){
            return $exception->getMessage().$exception->getLine();
        }
    }

    public function uploadFileUsingAccessToken($fileName){
//        dd($request->fileToUpload);
        try {
            $service = new \Google_Service_Drive($this->gClient);
            $user=User::find(1);

            $this->gClient->setAccessToken(json_decode($user->access_token,true));

            if ($this->gClient->isAccessTokenExpired()) {

                // save refresh token to some variable
                $refreshTokenSaved = $this->gClient->getRefreshToken();
                // update access token
                $this->gClient->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
                // // pass access token to some variable
                $updatedAccessToken = $this->gClient->getAccessToken();
                // // append refresh token
                $updatedAccessToken['refresh_token'] = $refreshTokenSaved;
                //Set the new acces token
                $this->gClient->setAccessToken($updatedAccessToken);
                $user->access_token=json_encode($updatedAccessToken);
                $user->save();
            }
//            $access_token = json_decode($user->access_token);
//
//            $result = $this->CreateSpreadsheet('test_sheet',$access_token->access_token);



            $fileMetadata = new Google_Service_Drive_DriveFile(array(
                'name' => 'ExpertPHP',
                'mimeType' => 'application/vnd.google-apps.folder'));
            $folder = $service->files->create($fileMetadata, array(
                'fields' => 'id'));
            printf("Folder ID: %s\n", $folder->id);


            $file = new Google_Service_Drive_DriveFile(array(
                'name' => $fileName,
                'parents' => array($folder->id)
            ));
            $result = $service->files->create($file, array(
                'data' => file_get_contents(public_path('uploadFiles/'.$fileName)),
                'mimeType' => 'application/octet-stream',
                'uploadType' => 'media'
            ));
            // get url of uploaded file
            $url='https://drive.google.com/open?id='.$result->id;
            dd($url);
        }catch (\Exception $exception){
            dd($exception);
        }


    }

    function CreateSpreadsheet($spreadsheet_title, $access_token) {
        $curlPost = array('properties' => array('title' => $spreadsheet_title));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://sheets.googleapis.com/v4/spreadsheets');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

        if($http_code != 200)
            exit('Error : Failed to create spreadsheet');

        return array('spreadsheet_id' => $data['spreadsheetId'], 'spreadsheet_url' => $data['spreadsheetUrl']);
    }

    public function getGoogleSheetData(){

        $service = new Google_Service_Sheets($this->gClient);
        $getrange = 'A:F';
        $data = $service->spreadsheets_values->get($this->readSheetId, $getrange);

        $values = $data->getValues();

        if (empty($values)) {
            print "No data found.\n";
        } else {
            return $values;
        }
    }

    public static function appendSheetData($service, $sheetId, $sheetColumns, $data){


        $options = array('valueInputOption' => 'RAW');
        $body   = new Google_Service_Sheets_ValueRange(['values' => $data]);

        $result = $service->spreadsheets_values->append($sheetId, $sheetColumns, $body, $options);
        return true;

    }

//    public function uploadSheet(Request $request){
//        $client = new Google_Client();
//        $client->setClientId('1020580055552-3mt8d0ghvdvreuctrh6m391ff9u019s2.apps.googleusercontent.com');
//        $client->setClientSecret('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');
//        $redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'],
//            FILTER_SANITIZE_URL);
//        $client->setRedirectUri($redirect);
//        $client->setScopes(array('https://www.googleapis.com/auth/drive'));
//
//        if(empty($_GET['code']))
//        {
//            $client->authenticate();
//        }
//
//        if(!empty($_FILES["fileToUpload"]["name"]))
//        {
//            $target_file=$_FILES["fileToUpload"]["name"];
//            // Create the Drive service object
//            $accessToken = $client->authenticate($_GET['code']);
//            $client->setAccessToken($accessToken);
//            $service = new Google_DriveService($client);
//            // Create the file on your Google Drive
//            $fileMetadata = new Google_Service_Drive_DriveFile(array(
//                'name' => 'My file'));
//            $content = file_get_contents($target_file);
//            $mimeType=mime_content_type($target_file);
//            $file = $driveService->files->create($fileMetadata, array(
//                'data' => $content,
//                'mimeType' => $mimeType,
//                'fields' => 'id'));
//            printf("File ID: %sn", $file->id);
//        }
//    }
}
